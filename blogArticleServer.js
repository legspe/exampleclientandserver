//code from various parts of expressjs.com (the getting started section
//and http://expressjs.com/en/4x/api.html and Database Intergration section)
//I also used code from this tutorial: http://blog.modulus.io/nodejs-and-sqlite
// and this one: https://scotch.io/tutorials/use-expressjs-to-get-url-and-post-parameters
'use strict'
//initialisation of express:
var express = require('express');
var app = express();

//listening on port 8080
app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});

var student = {
	email:'test@test.org',
	firstName: 'Beth',
	surname: 'Fields',
	subject: 'Fine Art'
};

/* this runs when a get request is received, so it sends the file asked for (here
we call it :name) */
app.get('/:name', function (req, res) {
  var options = {
		root:'public'
	};
  var fileName = req.params.name;
  if (fileName == 'requestStudent') {
    var studentJson = JSON.stringify(student);
    res.send(studentJson);
  }
  else {
    res.sendFile(fileName, options, function (err) {
    if (err) {
      res.status(err.status).sendFile('404.html',options);
    }
    });
  }
});

# README #

This repository contains a server and some web pages. It demonstrates how to send JavaScript objects from the server to the client, using AJAX techniques and JSON text. Node.js and Express are used as frameworks. Use 'npm install' to install the required packages and 'node blogArticleServer.js' to run the server.
